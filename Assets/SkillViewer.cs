﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillViewer : MonoBehaviour
{
    public static List<Skill> AllSkills;
    public List<GameObject> SkillsPrefabs;
    private static int LastID;
	// Use this for initialization
	void Start () {
        SkillsPrefabs = new List<GameObject>();
        AllSkills = new List<Skill>();

        Skill Going = new Skill("Going", LastID++,Skill.SkillType.Basic);//Скилл перемещение и его регистрация в менеджере
        RegisterSkill(Going);
	}

    void RegisterSkill(Skill Skill)
    {
        if(!AllSkills.Contains(Skill))
        {
            Debug.Log("Skill with this name: " + Skill.Name + " was loaded");
            AllSkills.Add(Skill);
        }
        else
        {
            Debug.Log("Skill with this name: "+Skill.Name+" exist");
        }
    }
}
