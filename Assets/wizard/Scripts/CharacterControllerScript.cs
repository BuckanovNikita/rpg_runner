﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System;

public class CharacterControllerScript : MonoBehaviour
{
    public delegate void VoidDelegate();//делегаты для событий 
    public delegate void Void_INTDeleagte(int a);

    public static event VoidDelegate OnShootEnd;//окончание анимации стрельбы файерболом  
    public static event VoidDelegate OnIntervalComplete;//прохождение этапа
    public static event VoidDelegate OnPlayerDeath;
   
    public static event Void_INTDeleagte OnScoreUpdate;
    private Animator Anim;//ссылка на компонент анимаций     
    public float MaxSpeed;//Максимальная скорость персонажа          
    private bool IsFacingRight = true; //переменная для определения направления персонажа вправо/влево   

    public GameObject FireBall;//префаб файербола
    public GameObject RightPointer;//объект для определения "прямо"
    public Vector3 FireballPosition;//начальное смещеине файербола
    private bool Shooting;//персонаж стреляет 
    private bool StartShooting;//персонаж начал стрельбу
    public float FireBallSpeed;//максимальная сила передаваемая файерболу    

    public Camera MainCamera;//ссылка на камеру
    public  List<GameObject> Tiles1;
    public  List<GameObject> Tiles2;
    public  List<GameObject> Tiles3;
    public  List<List<GameObject>> Tiles;
    private Vector3 Bottom_left;//левый нижний угол экрана  
    private Vector3 Bottom_right;//правый верхний угол экрана
    public int UnitsInTile = 4;//количество метров в одном тайле текстуры     
    private float Start_x;//начало отсчёта по х 
    private float Stop_x;//конец отсчета по x для расстановкм фона
    private float Delta_x = 0;//смещение за период между FixedUpdate
    private Vector3 PrevUpdatePosition;//положение в предыдущий FixedUpdate   
    private int CurrentLine;  
    public float MaxJumpForce;//MaxJumpForce

    private int Distance;//максимальное расстояние от старта до игрока(счёт)
    private Vector3 StartPosition;//стартовая позиция                       
    private float TmpDistance;//текущее расстояние от начала до игрока
    private int LastInterval;//расстояние от начала до стены
    public static int IntervalSize=20;//интервал между стенамиы

    private GroundCollisionChecker GCCheck;//компонент проверки нахождения персонажа на земле

    public int MaxHP;//Максимальное количество HP
    public int CurrentHP;//Текущее
    public GameObject[] HPIndicator;//Массив объектов отвечающих за индикацию HP
    private float DotRight;

    public float DotRight1
    {
        get
        {
            return DotRight;
        }
    }

    /// <summary>
    /// Начальная инициализация
    /// </summary>
    private void Start()
    {
        Anim = GetComponent<Animator>();
        LastInterval = 0;

        Tiles = new List<List<GameObject>>();
        Tiles.Add(Tiles1);
        Tiles.Add(Tiles2);
        Tiles.Add(Tiles3);

       
        OnShootEnd += Shoot;
        Shooting = Anim.GetBool("Shooting");

        Bottom_left = MainCamera.ScreenToWorldPoint(new Vector3(0, 0, 1));
        Bottom_right = MainCamera.ScreenToWorldPoint(new Vector3(Screen.width, 0, 1));
        Bottom_left -= 10* UnitsInTile * Vector3.right;
        Bottom_right += 10 * UnitsInTile * Vector3.right;
        Bottom_left += 2.5f * UnitsInTile*Vector3.up;
        Bottom_right += 2.5f * UnitsInTile * Vector3.up;
        Start_x = Bottom_left.x;
        Stop_x = Bottom_right.x;
        CurrentLine = 0;

        while (Start_x<Stop_x)
        {
            Instantiate(Tiles[0][CurrentLine],Bottom_left, Quaternion.identity);
            Instantiate(Tiles[1][CurrentLine],Bottom_left+Vector3.down*UnitsInTile, Quaternion.identity);
            Instantiate(Tiles[2][CurrentLine], Bottom_left + Vector3.down * 2*UnitsInTile, Quaternion.identity);
            Bottom_left += UnitsInTile * Vector3.right;
            Start_x = Bottom_left.x;
            CurrentLine += 1;
            CurrentLine %= 3;
        }
        StartPosition = transform.position;
        PrevUpdatePosition = this.gameObject.transform.position;

        GCCheck = gameObject.GetComponentInChildren<GroundCollisionChecker>();

        CurrentHP = MaxHP;
        OnPlayerDeath += LoadRecordScene;
    }
    /// <summary>
    /// Выполняем действия в методе FixedUpdate, т. к. в компоненте Animator персонажа
    /// выставлено значение Animate Physics = true и анимация синхронизируется с расчетами физики
    /// </summary>
    private void FixedUpdate()
    {

        DotRight = Vector2.Dot(Vector2.right, -this.transform.position + RightPointer.transform.position);
        //используем Input.GetAxis для оси Х. метод возвращает значение оси в пределах от -1 до 1.
        //при стандартных настройках проекта 
        //-1 возвращается при нажатии на клавиатуре стрелки влево (или клавиши А),
        //1 возвращается при нажатии на клавиатуре стрелки вправо (или клавиши D)
        Anim.SetBool("Grounded",GCCheck.Grounded);
       
        ScoreControl();
        Delta_x += (transform.position.x - PrevUpdatePosition.x);
        while(Delta_x>=UnitsInTile)
        {
            Instantiate(Tiles[0][CurrentLine], Bottom_left, Quaternion.identity);
            Instantiate(Tiles[1][CurrentLine], Bottom_left + Vector3.down * UnitsInTile, Quaternion.identity);
            Instantiate(Tiles[2][CurrentLine], Bottom_left + Vector3.down * 2 * UnitsInTile, Quaternion.identity);

            Bottom_left += UnitsInTile * Vector3.right;
            Start_x = Bottom_left.x;
            Delta_x -= UnitsInTile;

            CurrentLine += 1;
            CurrentLine %= 3;
        }
        float move = Input.GetAxisRaw("Horizontal");
        float shoot = Input.GetAxisRaw("Fire1");
        float JumpForce = Input.GetAxis("Jump");
        if ((Mathf.Abs(shoot) == 1.0f )&& !Shooting)
        { 
            Shooting = true;
            Anim.SetBool("Shooting", Shooting);
        }
        if((JumpForce!=0)&&GCCheck.Grounded)
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * MaxJumpForce * JumpForce);
            Anim.SetBool("NeedJump", true);
        }
        //в компоненте анимаций изменяем значение параметра Speed на значение оси Х.
        //приэтом нам нужен модуль значения
        Anim.SetFloat("Speed", Mathf.Abs(move));
        //обращаемся к компоненту персонажа RigidBody2D. задаем ему скорость по оси Х, 
        GetComponent<Rigidbody2D>().velocity = new Vector2(move * MaxSpeed,0);
        //если нажали клавишу для перемещения вправо, а персонаж направлен влево0
        if (move > 0 && !IsFacingRight)
            //отражаем персонажа вправо
            Flip();
        //обратная ситуация. отражаем персонажа влево
        else if (move < 0 && IsFacingRight)
            Flip();
        PrevUpdatePosition = transform.position;
        IntervalControl();
    }
    /// <summary>
    /// Контроль счёта игрока
    /// </summary>
    private void ScoreControl()
    {
        TmpDistance = Vector3.Distance(transform.position, StartPosition);
        if (TmpDistance > Distance)
        {
            OnScoreUpdate((int)TmpDistance);
            Distance = (int)TmpDistance;
            OnScoreUpdate(Distance);
        }
    }
    /// <summary>
    ///Расстановка Gameobject-ов по сетке
    /// </summary>
    void InstantieteWithStepByX(GameObject prefab,Vector3 StartPosition,Vector3 EndPosition,float Step)
    {

    }
    /// <summary>
    /// Контроль прохождения интервала
    /// </summary>
    private void IntervalControl()
    {
        while (Distance > LastInterval + IntervalSize)
        {
            LastInterval += IntervalSize;
            OnIntervalComplete();
        }
    }
    /// <summary>
    /// метод для контроля HP
    /// </summary>
    void HPController(WeaponData weapon)
    {
        for(int i=0;i<weapon.damage;i++)
        {
            HPIndicator[CurrentHP-1].SetActive(false);
            CurrentHP--;
            if (CurrentHP <= 0)
            {
                OnPlayerDeath();
                break;
            }
        }
        Anim.SetBool("HaveDamage", true);   
    }
    /// <summary>
    /// Окончание получения урона вызывается animation controller-ом
    /// </summary>
    void OnDamageAnimationEnd()
    {
        Anim.SetBool("HaveDamage", false);
    }

    /// <summary>
    /// Метод для смены направления движения персонажа и его зеркального отражения
    /// </summary>
    private void Flip()
    {
        //меняем направление движения персонажа
        IsFacingRight = !IsFacingRight;
        //получаем размеры персонажа
        Vector3 theScale = transform.localScale;
        //зеркально отражаем персонажа по оси Х
        theScale.x *= -1;
        //задаем новый размер персонажа, равный старому, но зеркально отраженный
        transform.localScale = theScale;
    }
    /// <summary>
    /// Вызов события генерируемый Animationcontroller
    /// </summary>
    void OnShootEndFunction()
    {
        OnShootEnd();
    }
    /// <summary>
    /// Разрешает снова сделать  прыжок
    /// </summary>
    void ChangeNeedJump()
    {
        Anim.SetBool("NeedJump", false);
    }
    /// <summary>
    /// Метод отвечающий за стрельбу  файерболлами
    /// </summary>
    void Shoot()
    {
        GameObject FireBallGo = Instantiate(FireBall,RightPointer.transform.position, Quaternion.identity);
        float dot = Vector2.Dot(Vector2.right, -this.transform.position + RightPointer.transform.position);
        FireBallGo.GetComponent<Rigidbody2D>().velocity =Vector2.right*FireBallSpeed*dot/Mathf.Abs(dot);
        Shooting = false;
        Anim.SetBool("Shooting", Shooting);
    }
    /// <summary>
    /// Отписка от событий при разрушении
    /// </summary>
    void OnDestroy()
    {
        OnShootEnd -= Shoot;
    }
    /// <summary>
    /// Контоль различных столкновений
    /// </summary>
    void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log("Collsion");
        if ( collision.gameObject.GetComponent<WeaponData>()!=null)
        {
           
            HPController(collision.gameObject.GetComponent<WeaponData>());
        }
    }
    void LoadRecordScene()
    {
        //Debug.Log("Death");
        RecordClass Record = new RecordClass(Distance);
        Record.SaveToPlayerPref();
        SceneManager.LoadScene("record_scene", LoadSceneMode.Single);
    }
}