﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballScript : MonoBehaviour {
    public float DestroyDistance;
    private GameObject player;
    private GameObject Wall;
    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        Wall = GameObject.FindGameObjectWithTag("Wall");
    }
	
	// Update is called once per frame
	void FixedUpdate (){
        if (Vector3.Distance(this.gameObject.transform.position, player.transform.position) > DestroyDistance)
            Destroy(gameObject);
        if((transform.position.x-Wall.transform.position.x)<1f)
        {
            Destroy(gameObject);
        }
    }
    void OnCollisionEnter2D()
    {
        Destroy(this.gameObject);
    }
}
