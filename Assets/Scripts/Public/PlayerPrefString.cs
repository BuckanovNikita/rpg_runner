﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefString  {
    public static string RecordCount = "RecordCount";
    public static string Distance = "Distance";
    public static string Level = "Level";
    public static string Experience = "Experience";
    public static string SkillPoints = "SP";
}
