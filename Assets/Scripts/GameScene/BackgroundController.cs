﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundController : MonoBehaviour {

    public GameObject[] Background;
    public int UnitsInbackgroundTile = 4;
    public int UnitsInTiles = 1;
    public int Depth;
    private Vector3 TopLeft;
    private Vector3  BottomLeft;
    private Camera _Camera;
    private float RealScreenHeight;
    private float BackgroundHeight;
    private int Line;
	// Use this for initialization
	void Start ()
    {
        _Camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        TopLeft = _Camera.ScreenToWorldPoint(new Vector3(0, Screen.height, 0));
        BottomLeft = _Camera.ScreenToWorldPoint(Vector3.zero);
        RealScreenHeight = TopLeft.y - BottomLeft.y;
        BackgroundHeight = RealScreenHeight - 3 * UnitsInTiles;
        //Debug.Log(RealScreenHeight);
        //Debug.Log(BackgroundHeight);
        foreach (GameObject x in Background)
        {
            x.transform.localScale = Vector3.one;
            x.transform.localScale =new Vector3(x.transform.localScale.x, BackgroundHeight*x.transform.localScale.y / UnitsInbackgroundTile, x.transform.localScale.z);
        }
        TopLeft += Depth*Vector3.forward;
        TopLeft += 0.5f*UnitsInbackgroundTile * Vector3.left;
        InstantiateBackground(TopLeft-CharacterControllerScript.IntervalSize*Vector3.right,transform.position.x + 2*CharacterControllerScript.IntervalSize,0);
        CharacterControllerScript.OnIntervalComplete += OnIntervalComplete;
            
    }

    void OnIntervalComplete()
    {
        InstantiateBackground(TopLeft, transform.position.x+2* CharacterControllerScript.IntervalSize, Line); //transform.position.x + CharacterControllerScript.IntervalSize, Line);
    }
    void InstantiateBackground(Vector3 StartPosition,float End,int line)
    {
        float Start = StartPosition.x;
        while (Start < End)
        {
            Instantiate(Background[line], StartPosition, Quaternion.identity);
            line++;
            line %= 3;
            StartPosition += UnitsInbackgroundTile * Vector3.right;
            Start += UnitsInbackgroundTile;
        }
        TopLeft = StartPosition;
        Line = line;
    }
    void OnDesttroy()
    {
        CharacterControllerScript.OnIntervalComplete -= OnIntervalComplete;
    }
}
