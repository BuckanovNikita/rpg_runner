﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameSceneController : MonoBehaviour {
    public GameObject MenuPanel;
    public List<GameObject> HiddenOnStopUI;
    public void Start()
    {
        MenuPanel.SetActive(false);
        foreach (GameObject x in HiddenOnStopUI)
        x.SetActive(true);
    }
    public void OnStopButtonClick()
    {
        Debug.Log("Stop_Button_Click");
        if (MenuPanel.activeInHierarchy)
            Time.timeScale = 1;
        else
            Time.timeScale = 0;
        MenuPanel.SetActive(!MenuPanel.activeInHierarchy);
        foreach (GameObject x in HiddenOnStopUI)
            x.SetActive(!x.activeSelf);
    }
    public void OnContiniueButtonClick()
    {
        Time.timeScale = 1;
        MenuPanel.SetActive(!MenuPanel.activeInHierarchy);
        foreach (GameObject x in HiddenOnStopUI)
            x.SetActive(!x.activeSelf);
    }
    public void OnEndRunButton()
    {
        SceneManager.LoadScene("main_menu_scene", LoadSceneMode.Single);
        OnContiniueButtonClick();
    }
}
