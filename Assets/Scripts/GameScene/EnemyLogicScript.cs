﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLogicScript : MonoBehaviour
{

    private Animator Anim;//ссылка на компонент анимаций     
    private GameObject Player;
    public float MaxSpeed;//Максимальная скорость персонажа          
    private bool IsFacingRight = true; //переменная для определения направления персонажа вправо/влево   

    public GameObject FireBall;//префаб файербола
    public GameObject RightPointer;//объект для определения "прямо"
    public Vector3 FireballPosition;//начальное смещеине файербола
    private bool Shooting;//персонаж стреляет 
    private bool StartShooting;//персонаж начал стрельбу
    public float FireBallSpeed;//максимальная сила передаваемая файерболу    

    private GroundCollisionChecker GCCheck;//компонент проверки нахождения персонажа на земле
    private CharacterControllerScript PlayerController;

    public int MaxHP;//Максимальное количество HP
    public int CurrentHP;//Текущее
    public GameObject[] HPIndicator;//Массив объектов отвечающих за индикацию HP

    public delegate void VoidDelegate();
    private VoidDelegate OnShootEnd;
    private float DotToRight;
    private float DotToPlayer;
    /// <summary>
    /// Начальная инициализация
    /// </summary>
    private void Start()
    {
        Anim = GetComponent<Animator>();
        GCCheck = gameObject.GetComponentInChildren<GroundCollisionChecker>();
        CurrentHP = MaxHP;
        Player = GameObject.FindGameObjectWithTag("Player");
        DotToRight = Vector2.Dot(Vector2.right, -this.transform.position + RightPointer.transform.position);
        DotToPlayer = Vector2.Dot(-Player.transform.position + transform.position, RightPointer.transform.position - transform.position);
        PlayerController = Player.GetComponent<CharacterControllerScript>();
    }
    /// <summary>
    /// Выполняем действия в методе FixedUpdate, т. к. в компоненте Animator персонажа
    /// выставлено значение Animate Physics = true и анимация синхронизируется с расчетами физики
    /// </summary>
    private void FixedUpdate()
    {
        Anim.SetBool("Grounded", GCCheck.Grounded);
        WizardLogic();
    }
    void WizardLogic()
    {

        if (Vector3.Distance(Player.transform.position, this.transform.position) < 0.2)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(MaxSpeed, 0) * -1 * DirectionToPlayer();
        }
        else
        {
                
            if (!Shooting)
                Anim.SetBool("Shooting", true);
        }
        Anim.SetFloat("Speed", Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x));

        DotToPlayer = Vector2.Dot(-Player.transform.position + transform.position, RightPointer.transform.position - transform.position);
        if (DotToPlayer>0)
        {
            Flip();
        }
        //Debug.Log("Enemy"+DotToRight.ToString());
        //Debug.Log("player"+Player.GetComponent<CharacterControllerScript>().DotRight1.ToString());
    }
    int DirectionToPlayer()
    {
        if (Player.transform.position.x > transform.position.x)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }
    /// <summary>
    /// метод для контроля HP
    /// </summary>
    void HPController(WeaponData weapon)
    {
        for (int i = 0; i < weapon.damage; i++)
        {
            HPIndicator[CurrentHP - 1].SetActive(false);
            CurrentHP--;
            if (CurrentHP <= 0)
            {
                Destroy(this.gameObject);
            }
        }
        Anim.SetBool("HaveDamage", true);
    }
    /// <summary>
    /// Окончание получения урона вызывается animation controller-ом
    /// </summary>
    void OnDamageAnimationEnd()
    {
        Anim.SetBool("HaveDamage", false);
    }

    /// <summary>
    /// Метод для смены направления движения персонажа и его зеркального отражения
    /// </summary>
    private void Flip()
    {
        //меняем направление движения персонажа
        IsFacingRight = !IsFacingRight;
        //получаем размеры персонажа
        Vector3 theScale = transform.localScale;
        //зеркально отражаем персонажа по оси Х
        theScale.x *= -1;
        //задаем новый размер персонажа, равный старому, но зеркально отраженный
        transform.localScale = theScale;
    }

    /// <summary>
    /// Разрешает снова сделать  прыжок
    /// </summary>
    void ChangeNeedJump()
    {
        Anim.SetBool("NeedJump", false);
    }
    /// <summary>
    /// Метод отвечающий за стрельбу  файерболлами
    /// </summary>
    void Shoot()
    {
        GameObject FireBallGo = Instantiate(FireBall, RightPointer.transform.position, Quaternion.identity);
        FireBallGo.GetComponent<Rigidbody2D>().velocity = DirectionToPlayer() * Vector2.right * FireBallSpeed;
        Shooting = false;
        Anim.SetBool("Shooting", Shooting);
    }
    /// <summary>
    /// Отписка от событий при разрушении
    /// </summary>
    void OnDestroy()
    {

    }
    /// <summary>
    /// Контоль различных столкновений
    /// </summary>
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<WeaponData>() != null)
        {

            HPController(collision.gameObject.GetComponent<WeaponData>());
        }
    }
}