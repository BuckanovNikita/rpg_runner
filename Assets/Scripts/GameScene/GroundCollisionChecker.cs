﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCollisionChecker : MonoBehaviour {

    public LayerMask GroundLayers;
    public float GroundRadius;
    private  bool _Grounded;
    public bool Grounded
    {
        get
        {
            return _Grounded;
        }
    }

    // Use this for initialization
    void Start () {
       _Grounded = true;
	}
    void FixedUpdate()
    {
        _Grounded = Physics2D.OverlapCircle(transform.position, GroundRadius, GroundLayers);
    }
}
