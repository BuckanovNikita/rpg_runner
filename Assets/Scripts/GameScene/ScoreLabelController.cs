﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreLabelController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        CharacterControllerScript.OnScoreUpdate += ScoreUpdate;
	}

    private void ScoreUpdate(int a)
    {
        GetComponent<Text>().text = a.ToString();
    }

    void OnDestroy()
    {
        CharacterControllerScript.OnScoreUpdate -= ScoreUpdate;
    }
}
