﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteControllerScript : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        WallContoller.OnWallGo += CheckDestroy;
    }
    void CheckDestroy(float x)
    {

        if (transform.position.x < x)
        {
            WallContoller.OnWallGo -= CheckDestroy;
            Destroy(gameObject);
            
        }
    }
    void OnDestroy()
    {
        WallContoller.OnWallGo -= CheckDestroy;
    }
}
