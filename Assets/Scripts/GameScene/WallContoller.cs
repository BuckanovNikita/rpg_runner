﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallContoller : MonoBehaviour {
    private Vector3 CurrentPosition;
    private Vector3 PreviousePosition;
    public delegate void  Void_IntDelegate(float x);
    public static event Void_IntDelegate OnWallGo;
	// Use this for initialization
	void Start () {
        CurrentPosition = transform.position;
        PreviousePosition = CurrentPosition - CharacterControllerScript.IntervalSize * Vector3.left;
        CharacterControllerScript.OnIntervalComplete += Wall;
	}
	
	// Update is called once per frame
	void Update () {
	}
    void Wall()
    {
        PreviousePosition = CurrentPosition;
        CurrentPosition = transform.position + CharacterControllerScript.IntervalSize * Vector3.right;
        transform.position = CurrentPosition;
        OnWallGo(PreviousePosition.x);
    }
    void OnDestroy()
    {
        CharacterControllerScript.OnIntervalComplete -= Wall;
    }
}
