﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecordClass:IComparer<RecordClass>,IComparer,IComparable<RecordClass>
{ 
    private int Distance;

    public int PDistance
    {
        get
        {
            return Distance;
        }
    }

    public RecordClass(int d)
    {
        Distance = d;
    }
    public void SaveToPlayerPref()
    {
        if (!PlayerPrefs.HasKey(PlayerPrefString.RecordCount))
        {

            PlayerPrefs.SetInt(PlayerPrefString.RecordCount, 1);
        }
        else
            PlayerPrefs.SetInt(PlayerPrefString.RecordCount, PlayerPrefs.GetInt(PlayerPrefString.RecordCount) + 1);
        PlayerPrefs.SetInt(PlayerPrefString.Distance+PlayerPrefs.GetInt(PlayerPrefString.RecordCount).ToString(), Distance);
        PlayerPrefs.Save();
    }
    public static List<RecordClass> LoadFromPlayerPref(int Start,int End)
    {
        List<RecordClass> Result = new List<RecordClass>();
        
        if(PlayerPrefs.HasKey(PlayerPrefString.RecordCount))
        {
            int RecordCount = PlayerPrefs.GetInt(PlayerPrefString.RecordCount);
            if (Start < RecordCount)
            {
                for (int i = 1; i <= RecordCount; i++)
                {
                    Result.Add(new RecordClass(PlayerPrefs.GetInt(PlayerPrefString.Distance + i.ToString())));
                }
                Result.Sort();
                Result.RemoveRange(0, Start);
                Result.RemoveRange(Mathf.Min(End - Start, Result.Count), Mathf.Max(0, Result.Count + Start - End));
                return Result;
            }
            else
                return null;
        }
        else
        {
            return null;
        }
    }

    public int Compare(RecordClass x, RecordClass y)
    {
        if (x.PDistance == y.PDistance)
            return 0;
        else if (x.PDistance > y.PDistance)
            return 1;
        else 
            return -1;
    }

    public int Compare(object x, object y)
    {
        if (((RecordClass)x).PDistance == ((RecordClass)y).PDistance)
            return 0;
        else if (((RecordClass)x).PDistance > ((RecordClass)y).PDistance)
            return -1;
        else
            return 1;
    }

    public int CompareTo(RecordClass other)
    {
        if (PDistance == other.PDistance)
            return 0;
        else if (PDistance > other.PDistance)
            return -1;
        else
            return 1;
    }
}
