﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RecordSceneController : MonoBehaviour {
    public int CountOnScreen;
    public GameObject[] RecordLabels;
    private int StartValue;
    private int EndValue;
    void Start()
    {
        StartValue = 0;
        EndValue = CountOnScreen;
        Draw(StartValue);
    }
    public void OnBackButtonClick()
    {
        SceneManager.LoadScene("main_menu_scene", LoadSceneMode.Single);
    }
    public void OnNextRecordsButtonClick()
    {
        StartValue += CountOnScreen;
        EndValue += CountOnScreen;
        Draw(StartValue);
    }
    void Draw(int Start)
    {
        List<RecordClass> records = RecordClass.LoadFromPlayerPref(Start,Start+CountOnScreen);
        if (records != null)
        {
            for (int i = Start; i < Start + Mathf.Min(records.Count, CountOnScreen); i++)
            {
                Text[] texts = RecordLabels[i - Start].GetComponentsInChildren<Text>();
                texts[0].text = (i + 1).ToString();
                texts[1].text = records[i - Start].PDistance.ToString();
            }
        for(int i=Start+records.Count;i<Start+CountOnScreen;i++)
        {
            Text[] texts = RecordLabels[i - Start].GetComponentsInChildren<Text>();
            texts[0].text = "";
            texts[1].text = "";
        }
        }
    }
}
