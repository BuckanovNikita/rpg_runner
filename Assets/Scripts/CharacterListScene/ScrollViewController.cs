﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollViewController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        CharacterIconController.OnCharacterButtonClick += OnCharacterChange;
	}
    void OnCharacterChange(CharacterInfoScript Info,GameObject obj)
    {
        this.gameObject.SetActive(false);
    }
    void OnDestroy()
    {
        CharacterIconController.OnCharacterButtonClick -= OnCharacterChange;
    }
}
