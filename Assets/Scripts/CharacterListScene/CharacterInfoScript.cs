﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System;
using System.IO;

[Serializable]
public class CharacterInfoScript:IEquatable<CharacterInfoScript>
{
    private string Name;//имя   
    private int Level;//Уровень
    private int Experience;//Опыт
    private List<Skill> AviableSkills;//Изученные навыки
    private List<Item> Items;//Доступные предметы
    private int SkillPoints;//Количество доступныз очков навыков
    private float Speed;//Стандартная скорость
    private int MaxHP;//максимум HP
    public static string FileName = "Characters.dat";

    public string GetName
    {
        get
        {
            return Name;
        }
    }

    public CharacterInfoScript(string name)
    {
        Name = name;
        Level = 1;
        Experience = 0;
        AviableSkills = null;
        Items = null;
        SkillPoints = 1;
        Speed = 4;
        MaxHP = 4;
        AviableSkills = SkillViewer.AllSkills.FindAll(x => x.Type == Skill.SkillType.Basic);
    }
    public static void Save(List<CharacterInfoScript> Info,bool LoadOld=true)
    {
        List<CharacterInfoScript> list;
        if (LoadOld)
        {
            list = Load();
            foreach (CharacterInfoScript x in Info)
            {
                list.Add(x);
            }
        }
        else
        {
            list = Info;
        }
        BinaryFormatter Formatter = new BinaryFormatter();
        Stream fStream = new FileStream(FileName, FileMode.Create, FileAccess.Write, FileShare.None);
        Formatter.Serialize(fStream, list);
        fStream.Close();
    }
    public static void Save(CharacterInfoScript Info)
    {
        List<CharacterInfoScript> list = CharacterInfoScript.Load();
        if (list == null)
            list = new List<CharacterInfoScript>();
        list.Add(Info);
        BinaryFormatter Formatter = new BinaryFormatter();
        Stream fStream = new FileStream(FileName, FileMode.Create, FileAccess.Write, FileShare.None);
        Formatter.Serialize(fStream, list);
        fStream.Close();
    }
   public static List<CharacterInfoScript> Load()
    {
        Stream fStream = new FileStream(FileName, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
        List<CharacterInfoScript> list = null;
        if (fStream.Length != 0)
            list = (List<CharacterInfoScript>)new BinaryFormatter().Deserialize(fStream);
        fStream.Close();
        return list;
    }
    public static void Remove(CharacterInfoScript Info)
    {
        List<CharacterInfoScript> list = Load();
        if (list != null)
            list.Remove(Info);
        if (list!=null)
            Save(list,false);
    }

    public bool Equals(CharacterInfoScript other)
    {
        return this.Name==other.Name;
    }
}
