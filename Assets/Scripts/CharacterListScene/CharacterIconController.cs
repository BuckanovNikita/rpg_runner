﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CharacterIconController : MonoBehaviour {
    private CharacterInfoScript CharacterInfo;
    public delegate void Void_CharacterInfoScriptGameobjectDelegate(CharacterInfoScript Info,GameObject obj);
    public static  event Void_CharacterInfoScriptGameobjectDelegate OnCharacterButtonClick;

    public void Start()
    {
        this.gameObject.GetComponent<Button>().onClick.AddListener(OnClick);
    }
    public void Initialize(CharacterInfoScript Info)
    {
        this.gameObject.GetComponentInChildren<Text>().text = Info.GetName;
        CharacterInfo = Info;
    }

    public void OnClick()
    {
        OnCharacterButtonClick(CharacterInfo,this.gameObject);
    }
}
