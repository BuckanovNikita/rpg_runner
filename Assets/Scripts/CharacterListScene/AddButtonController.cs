﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddButtonController : MonoBehaviour {

	// Use this for initialization
	void Awake () {
        CharacterListMenuController.OnNewCharacterAddButtonClick += MoveInHierarchy;
        CharacterListMenuController.OnCharactersAdd += MoveInHierarchy;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void MoveInHierarchy(int delta)
    {
        int index = transform.GetSiblingIndex();
        transform.SetSiblingIndex(index + delta);
    }
    void OnDestroy()
    {
        CharacterListMenuController.OnNewCharacterAddButtonClick -= MoveInHierarchy;
        CharacterListMenuController.OnCharactersAdd -= MoveInHierarchy;
    }
}
