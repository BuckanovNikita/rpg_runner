﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CharacterListMenuController : MonoBehaviour
{
    public GameObject AddNewPanel;
    public GameObject CharacterIconPrefab;
    public GameObject Content;
    private CharacterInfoScript CurrentCharacter;
    private GameObject CurrentCharacterButton;
    public delegate void Void_IntDelegate(int x);
    public static event Void_IntDelegate OnNewCharacterAddButtonClick;
    public static event Void_IntDelegate OnCharactersAdd;
    void Start()
    {
        List<CharacterInfoScript> list = CharacterInfoScript.Load();
        CharacterIconController.OnCharacterButtonClick += SetCurrentCharacter;
        AddCharacters(list);
    }
    public void OnStartGameButtonClick()
    {
        SceneManager.LoadScene("game_scene", LoadSceneMode.Single);
    }
    public void OnBackToMenuButton()
    {
        SceneManager.LoadScene("main_menu_scene", LoadSceneMode.Single);
    }
    public void OnAddNewButtonClick()
    {
        AddNewPanel.SetActive(true);
    }
    public void DeleteCurrentCharacter()
    {
        CharacterInfoScript.Remove(CurrentCharacter);
        Destroy(CurrentCharacterButton);
    }
    public void OnNewCharacterAddButtonClickFunction()
    {
        string name = "";
        name = GameObject.Find("NameInput").GetComponent<Text>().text;
        CharacterInfoScript Info = new CharacterInfoScript(name);
        if (! CharacterInfoScript.Load().Contains(Info))
        {
            CharacterInfoScript.Save(Info);
            AddCharacter(Info);
            AddNewPanel.SetActive(false);
            OnNewCharacterAddButtonClick(1);
        }
        else
        {
            Debug.Log("Персонаж с таким именем существует");
        }
    }

    public void AddCharacters(List<CharacterInfoScript> Characters)
    {
        if (Characters != null)
        {
            foreach (CharacterInfoScript Info in Characters)
            {
                GameObject Button = Instantiate(CharacterIconPrefab, Content.transform);
                Button.transform.localScale = Vector3.one;
                Button.GetComponent<CharacterIconController>().Initialize(Info);

            }
            if (Characters != null)
               OnCharactersAdd(Characters.Count);
        }

    }
    public void AddCharacter(CharacterInfoScript Info)
    {
        GameObject Button = null;
        if (Info != null)
        {
            Button = Instantiate(CharacterIconPrefab, Content.transform);
            Button.GetComponent<CharacterIconController>().Initialize(Info);
            Button.transform.localScale = Vector3.one;
            OnCharactersAdd(1);
        }
    }
    public void SetCurrentCharacter(CharacterInfoScript Info,GameObject obj)
    {
        CurrentCharacter = Info;
        CurrentCharacterButton = obj;
    }
}
