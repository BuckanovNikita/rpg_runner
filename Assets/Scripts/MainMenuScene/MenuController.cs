﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MenuController : MonoBehaviour {
    public void onNewgameButtonClick()//нажатие кнопки новая игра   
    {
        SceneManager.LoadScene("character_list_scene", LoadSceneMode.Single);
        //SceneManager.UnloadSceneAsync("game_scene");
    }
    public void onCharacterButtonClick()//сцена выбора и прокачки персонажа
    {
        SceneManager.LoadScene("character_list_scene", LoadSceneMode.Single);
    }
    public void onRecordButtonClick()//сцена с рекордами
    {
        SceneManager.LoadScene("record_scene", LoadSceneMode.Single);
    }
    public void onQuitButtonClick()//выход из игры
    {
        Application.Quit();
    }
}
