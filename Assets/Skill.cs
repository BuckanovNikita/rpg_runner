﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[Serializable]
public class Skill:IEquatable<Skill>
{
    public enum SkillType {Basic,Wizard};
    private String _Name;
    private int _ID;
    private SkillType _Type;
    public String Name
    {
        get { return _Name; }
        set { _Name = value; }
    }
    public SkillType Type
    {
        get
        {
            return _Type;
        }

        set
        {
            _Type = value;
        }
    }

    public Skill(String Name,int ID,SkillType Type)
    {
        _Name = Name;
        _ID = ID;
        _Type = Type;
    }
    public bool Equals(Skill other)
    {
        return this.Name == other.Name;
    }
}
